import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createI18n } from 'vue-i18n'
import EN from '@/i18n/en.json'
import RU from '@/i18n/ru.json'

import '@/assets/scss/app.scss'

import App from '@/App.vue'
import router from '@/router'

const app = createApp(App)

app.use(createPinia())

const i18n = createI18n({
  locale: 'RU',
  messages: {
    RU: RU,
    EN: EN
  }
})

app.use(i18n)
app.use(router)

app.mount('#app')
