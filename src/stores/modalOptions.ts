import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Order } from '@/interfaces/Order'

export const useModalOptions = defineStore('modal', () => {
  const isShow = ref(false)
  const currentItem = ref<Order | null>(null)
  const isActive = computed(() => isShow)
  const item = computed(() => currentItem)

  function open(item: Order | null) {
    isShow.value = !isShow.value
    currentItem.value = item
  }

  function close() {
    isShow.value = false
  }

  return {
    isShow,
    currentItem,
    open,
    close,
    isActive,
    item
  }
})
