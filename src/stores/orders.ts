import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useModalOptions } from '@/stores/modalOptions.js'
import type { Order } from '@/interfaces/Order'
import axios from 'axios'

export const useOrdersStore = defineStore('orders', () => {
  const { close } = useModalOptions()
  const orders = ref<Order[] | []>([])
  const loading = ref(true)
  const ordersList = computed(() => orders)
  const isLoading = computed(() => loading)

  async function getOrders() {
    try {
      const response = await axios.get<Order[]>(
        'https://stoplight.io/mocks/reelkrey/dzencode/244600806/orders'
      )
      orders.value = response.data
      loading.value = false
    } catch (e) {
      console.log(e)
    }
  }

  function deleteItem(item: Order | null) {
    if (item) {
      orders.value = orders.value.filter((order) => order.id !== item.id)
    }
    close()
  }

  return {
    getOrders,
    deleteItem,
    orders,
    ordersList,
    isLoading
  }
})
