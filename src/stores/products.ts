import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useModalOptions } from '@/stores/modalOptions.js'
import type { Order } from '@/interfaces/Order'
import axios from 'axios'

export const useProductsStore = defineStore('products', () => {
  const { currentItem, close } = useModalOptions()
  const products = ref<Order[] | []>([])
  const loading = ref(true)
  const productsList = computed(() => products)
  const isLoading = computed(() => loading)

  async function getProducts(id: number) {
    try {
      const response = await axios.get<Order[]>(
        `https://stoplight.io/mocks/reelkrey/dzencode/244600806/orders/${id}`,
        {
          headers: {
            Prefer: `code=200, example=Example ${id}`
          }
        }
      )
      products.value = response.data
      loading.value = false
    } catch (e) {
      console.log(e)
    }
  }

  function deleteItem() {
    if (currentItem) {
      products.value = products.value.filter((item) => item.id !== currentItem.id)
    }
    close()
  }

  function deleteProduct(product: Order) {
    products.value = products.value.filter((item) => item.id !== product.id)
  }

  return {
    getProducts,
    products,
    deleteItem,
    deleteProduct,
    productsList,
    isLoading
  }
})
