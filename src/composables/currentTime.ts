import { ref } from 'vue'
import { DateTime } from 'luxon'

export const useCurrentTime = () => {
  const currentTime = ref(DateTime.local().toFormat('HH:mm'))
  const currentDate = ref(DateTime.local().toFormat('d LLL, yyyy'))

  setInterval(() => {
    currentTime.value = DateTime.local().toFormat('HH:mm')
  }, 1000)

  return {
    currentTime,
    currentDate
  }
}
