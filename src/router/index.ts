import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'coming',
      component: () => import('@/views/Coming.vue')
    },
    {
      path: '/groups',
      name: 'groups',
      component: () => import('@/views/Groups.vue')
    },
    {
      path: '/orders/:id',
      name: 'orders',
      component: () => import('@/views/Products.vue')
    }
  ]
})

export default router
