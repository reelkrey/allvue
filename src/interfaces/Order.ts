export interface Order {
  amount: number
  date: string
  description: string
  guarantee: {
    start: string
    end: string
  }
  groupName: string
  id: number
  isNew: number
  price: {
    uah: string
    usd: string
  }
  photo: string
  serialNumber: number
  specification: string
  title: string
  type: string
}
