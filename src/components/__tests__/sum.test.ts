import { describe, expect, it } from 'vitest'
import sum from '@/composables/sum'

describe('#sum', () => {
  it('returns 0 whit no numbers', () => {
    expect(sum()).toBe(0)
  })

  it('returns same number with one number', () => {
    expect(sum(1)).toBe(1)
  })

  it('returns sum with multiple number', () => {
    expect(sum(1, 2, 3)).toBe(6)
  })
})
